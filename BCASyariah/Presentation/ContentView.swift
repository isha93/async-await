//
//  ContentView.swift
//  BCASyariah
//
//  Created by isa nur fajar on 23/03/22.
//

import SwiftUI

struct ContentView: View {
    @StateObject var contentVM : ContentViewModel = ContentViewModel()
    var body: some View {
        GeometryReader { geo in
            VStack(alignment: .center, spacing: 20){
                TextField("Email", text: $contentVM.userName)
                SecureField("Password", text: $contentVM.pass)
                Button {
                    Task.init {
                        try await contentVM.doLogin()
                    }
                } label: {
                    Text("Login")
                }
                .buttonStyle(.bordered)

            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
