//
//  ContentViewModel.swift
//  BCASyariah
//
//  Created by isa nur fajar on 23/03/22.
//

import Foundation

@MainActor
class ContentViewModel : ObservableObject {
    @Published var userName : String = ""
    @Published var pass : String = ""
    
    private var contentServices : ContentViewSerivcesProtocol
    private var prefs = UserDefaults.standard
    
    init(contentServices : ContentViewSerivcesProtocol = ContentViewSerivces()){
        self.contentServices = contentServices
    }
    
    func doLogin() async throws {
        do {
            let dataLogin = try await contentServices.doResetPass(endPoint: .doLogin(userName: userName, pass: pass))
            print(dataLogin)
        }catch{
            print(error)
        }
    }
}
