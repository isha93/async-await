//
//  Networker.swift
//  BCASyariah
//
//  Created by isa nur fajar on 23/03/22.
//

import Foundation
import netfox

protocol NetworkerProtocol: AnyObject {
    func taskAsync<T>(type: T.Type,
                      endPoint: NetworkFactory
    ) async throws -> T where T: Decodable
}

final class Networker: NetworkerProtocol {
    func taskAsync<T>(type: T.Type, endPoint: NetworkFactory) async throws -> T where T : Decodable {
        let (data, response) = try await URLSession.shared.data(for: endPoint.urlRequest)
        
        guard let httpResponse = response as? HTTPURLResponse else {
            throw NetworkError.middlewareError(code: 500, message: "Connection Error")
        }
        NFX.sharedInstance().start()
        let dataString = String(decoding: data, as: UTF8.self)
        print("Response : \(dataString)")
        
        guard 200..<300 ~= httpResponse.statusCode else {
            let res  = try JSONDecoder().decode(NetworkHandle.self,from:data)
            let error = self.mapHTTPError(with: res.status, errorMessage: res.message)
            throw error
        }
        
        do {
            let decoder = JSONDecoder()
            return try decoder.decode(type, from: data)
        } catch {
            print(error)
            throw NetworkError.middlewareError(code: 500, message: error.localizedDescription)
        }
    }
}

extension Networker {
    private func mapHTTPError(with code : Int, errorMessage: String) -> NetworkError {
        let error : NetworkError
        switch code {
        default:
            error = .middlewareError(code: code, message: errorMessage)
        }
        return error
    }
}

