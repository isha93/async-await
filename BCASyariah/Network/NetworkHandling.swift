//
//  NetworkHandling.swift
//  BCASyariah
//
//  Created by isa nur fajar on 23/03/22.
//

import Foundation

enum NetworkError: Error, LocalizedError {
    
    case middlewareError(code: Int, message: String?)
    
    var localizedDescription: String {
        switch self {
        case .middlewareError(_, let message):
            return message ?? ""
        }
    }
}

struct NetworkHandle: Decodable, Error, LocalizedError {
    let status: Int
    let message: String
}
