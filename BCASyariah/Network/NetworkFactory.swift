//
//  NetworkFactory.swift
//  BCASyariah
//
//  Created by isa nur fajar on 23/03/22.
//

import Foundation


enum NetworkFactory {
    case doLogin(userName : String , pass : String)
}

extension NetworkFactory {
    // MARK: Path of API
    var path : String {
        switch self {
        case .doLogin:
            return "/auth/login"
        }
    }
    
    // MARK: Query param in url
    var queryParam : [URLQueryItem]{
        switch self {
        case .doLogin:
            return []
        }
    }
    
    enum RequestMethod: String {
        case delete = "DELETE"
        case get = "GET"
        case patch = "PATCH"
        case post = "POST"
        case put = "PUT"
    }
    
    // MARK: Request Method
    var method : RequestMethod {
        switch self {
        case .doLogin:
            return .post
        }
    }
    
    // MARK: Body Parameters API
    var bodyParam : [String : Any]? {
        switch self {
        case .doLogin(let userName, let pass):
            return [
                "email":userName,
                "password":pass
            ]
        }
    }
    
    enum HeaderType {
        case anonymous
        case authorized
    }
    
    
    fileprivate func getHeaders(type: HeaderType) -> [String: String] {
        var header: [String: String]
        switch type {
        case .anonymous:
            header = [:]
        case .authorized:
            header = ["Content-Type": "application/json",
                      "Accept" : "*/*",
                      "Content-Length" : ""]
        }
        return header
    }
    
    // MARK: Header of API
    var headers : [String : String]? {
        switch self {
        case .doLogin:
            return getHeaders(type: .authorized)
        }
    }

    // MARK: Final URL of api
    var url : URL {
        let baseApi = "3.89.145.177:8000"
        var components = URLComponents()
        components.scheme = "http"
        components.host = baseApi
        components.path = path
        components.queryItems = queryParam
        
        print(components.url)
        
        guard let url = components.url else {
            preconditionFailure("Invalid URL components: \(components)")
        }
        
        return url
    }
    
    var urlRequest : URLRequest {
        var urlRequest = URLRequest(url: URL(string: "http://3.89.145.177:8000/auth/login")!)
        urlRequest.httpMethod = method.rawValue
        if let header = headers {
            header.forEach { key, value in
                urlRequest.setValue(value, forHTTPHeaderField: key)
            }
        }
        
        if let bodyParam = bodyParam {
            if method != .get {
                urlRequest.httpBody = try? JSONSerialization.data(withJSONObject: bodyParam)
            }
        }
        return urlRequest
    }
}
