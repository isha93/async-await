//
//  BCASyariahApp.swift
//  BCASyariah
//
//  Created by isa nur fajar on 23/03/22.
//

import SwiftUI
import netfox

@main
struct BCASyariahApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
                .onAppear {
                    NFX.sharedInstance().start()
                }
        }
    }
}
