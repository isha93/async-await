//
//  ContentViewServices.swift
//  BCASyariah
//
//  Created by isa nur fajar on 23/03/22.
//

import Foundation

protocol ContentViewSerivcesProtocol : AnyObject {
    var networker: NetworkerProtocol { get }
    func doResetPass(endPoint : NetworkFactory) async throws -> LoginModels
}

final class ContentViewSerivces : ContentViewSerivcesProtocol {
    var networker: NetworkerProtocol
    
    init(networker: NetworkerProtocol = Networker()) {
        self.networker = networker
    }
    
    func doResetPass(endPoint: NetworkFactory) async throws -> LoginModels {
        return try await networker.taskAsync(type: LoginModels.self, endPoint: endPoint)
    }
}
