//
//  ContentModels.swift
//  BCASyariah
//
//  Created by isa nur fajar on 23/03/22.
//

import Foundation
// MARK: - Welcome
struct LoginModels: Decodable {
    let status: Int
    let message: String
    let data: LoginData
}

// MARK: - DataClass
struct LoginData: Decodable {
    let hasPin: Bool
    let id: Int
    let email, token: String
    let expiredIn, expiredAt: Int
    let refreshToken: String
    let refreshTokenExpiredIn, refreshTokenExpiredAt: Int

    enum CodingKeys: String, CodingKey {
        case hasPin, id, email, token
        case expiredIn = "expired_in"
        case expiredAt = "expired_at"
        case refreshToken = "refreshToken"
        case refreshTokenExpiredIn = "refreshToken_expired_in"
        case refreshTokenExpiredAt = "refreshToken_expired_at"
    }
}
